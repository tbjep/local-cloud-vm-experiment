#!/bin/sh

sudo virt-install \
	--name=hal9000 \
	--ram=2048 \
	--cpu host \
	--vcpus=1 \
	--import \
	--disk path=/var/lib/libvirt/images/hal9000.qcow2,format=qcow2 \
	--os-variant rocky8.6 \
	--network bridge=virbr0,model=virtio \
	--graphics vnc,listen=0.0.0.0 \
	--noautoconsole \
	--cloud-init user-data=./user-data,meta-data=./meta-data


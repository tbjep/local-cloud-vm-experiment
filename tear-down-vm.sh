#!/bin/sh

[ $# -eq 1 ] || { echo "usage: $0 <name>" 1>&2; exit 1; }

NAME=$1

virsh shutdown $NAME

virsh undefine $NAME

DISK_NAME="/var/lib/libvirt/images/$NAME.qcow2"

rm -v $DISK_NAME

#!/bin/sh

[ $# -eq 1 ] || { echo "usage: $0 <name>" 1>&2; exit 1; }

NAME=$1

[ -f "/var/lib/libvirt/boot/Rocky-8-GenericCloud.latest.x86_64.qcow2" ] || curl --output-dir /var/lib/libvirt/boot/ -O https://dl.rockylinux.org/pub/rocky/8.6/images/Rocky-8-GenericCloud.latest.x86_64.qcow2

DISK_NAME="/var/lib/libvirt/images/$NAME.qcow2"

qemu-img create -b /var/lib/libvirt/boot/Rocky-8-GenericCloud.latest.x86_64.qcow2 -f qcow2 -F qcow2 "$DISK_NAME" 10G

export NAME
virt-install \
	--name=$NAME \
	--ram=2048 \
	--cpu host \
	--vcpus=1 \
	--import \
	--disk path="$DISK_NAME",format=qcow2 \
	--os-variant rocky8.6 \
	--network bridge=virbr0,model=virtio \
	--graphics vnc,listen=0.0.0.0 \
	--noautoconsole \
	--cloud-init user-data=./user-data,meta-data=<(envsubst < ./meta-data-template)

